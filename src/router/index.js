import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: '组件库',
    component: () => import('../views/comps/index'),
    redirect: '/borderBox',
    children: [
      {
        path: '/borderBox',
        name: 'borderBox 边框',
        meta: {},
        component: () => import('../views/comps/borderBox'),
      },{
        path: '/head',
        name: 'head 头部',
        meta: {},
        component: () => import('../views/comps/head'),
      },{
        path: '/background',
        name: 'background 背景',
        meta: {},
        component: () => import('../views/comps/background'),
      },{
        path: '/count',
        name: 'count 统计栏',
        meta: {},
        component: () => import('../views/comps/count'),
      },{
        path: '/table',
        name: 'table 表格',
        meta: {},
        component: () => import('../views/comps/table'),
      },{
        path: '/seamless',
        name: 'seamless 无缝滚动',
        meta: {},
        component: () => import('../views/comps/seamless'),
      },{
        path: '/chart',
        name: 'chart 图表',
        meta: {},
        component: () => import('../views/comps/chart'),
      },{
        path: '/layout',
        name: 'layout 布局',
        meta: {},
        component: () => import('../views/comps/layout'),
      },
    ]
  },{
    path: '/digitalTwins',
    name: '数字孪生',
    component: () => import('../views/dt/index'),
    redirect: '/wind',
    children: [
      {
        path: '/wind',
        name: '风场发电机',
        meta: {},
        component: () => import('../views/dt/wind'),
      },{
        path: '/Alone',
        name: 'Alone',
        meta: {
          hidden: true,
        },
        component: () => import('../views/dt/alone.vue')
      },{
        path: '/traffic',
        name: '智慧交通',
        meta: {},
        component: () => import('../views/dt/traffic'),
      },
      // {
      //   path: '/park',
      //   name: '智慧园区',
      //   meta: {},
      //   component: () => import('../views/dt/park'),
      // }
    ]
  },{
    path: '/cases',
    name: '案例库',
    component: () => import('../views/cases/index'),
    redirect: '/Taxation',
    children: [
      {
        path: '/Taxation',
        name: '大数据治税平台领导舱',
        meta: {},
        component: () => import('../views/cases/taxation'),
      },{
        path: '/Gridding',
        name: '衡阳网格化大屏',
        meta: {},
        component: () => import('../views/cases/gridding'),
      },{
        path: '/LargeTransport',
        name: '广东省大件运输',
        meta: {},
        component: () => import('../views/cases/largeTransport'),
      },{
        path: '/DataAnalysis',
        name: '公共资源交易总体数据分析',
        meta: {},
        component: () => import('../views/cases/dataAnalysis'),
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
