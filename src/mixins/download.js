import JSZip from "jszip";
import FileSaver from "file-saver";

export const download = {
    methods: {
        //文件以流的形式获取（参数url为文件链接地址）
        getImgArrayBuffer(url) {
            return new Promise((resolve, reject) => {
            //通过请求获取文件blob格式
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", url, true);
            xmlhttp.responseType = "blob";
            xmlhttp.onload = function () {
                if (xmlhttp.status == 200) {
                resolve(xmlhttp.response);
                } else {
                reject(xmlhttp.response);
                }
            };
            xmlhttp.send();
            });
        },
        downloadBtn(fileList, fileName) {
            var blogTitle = fileName; // 下载后压缩包的名称
            var zip = new JSZip();
            var promises = [];
            let cache = {};
            for (let item of fileList) {
              // item.feilePath为文件链接地址
              // item.fileName为文件名称
              if(item.feilePath) {
                const promise = this.getImgArrayBuffer(window.location.origin + this.baseUrl + item.feilePath).then((data) => {
                  // 下载文件, 并存成ArrayBuffer对象(blob)
                  zip.file(item.fileName, data, { binary: true }); // 逐个添加文件
                  cache[item.fileName] = data;
                });
                promises.push(promise);
              } else {
              // feilePath地址不存在时提示
                alert(`附件${item.fileName}地址错误，下载失败`);
              }
            }
            Promise.all(promises).then(() => {
              zip.generateAsync({ type: "blob" }).then((content) => {
                // 生成二进制流
                FileSaver.saveAs(content, blogTitle); // 利用file-saver保存文件  blogTitle:自定义文件名
              });
            }).catch((res) => {
              alert("文件压缩失败");
            });
        },
    }
}