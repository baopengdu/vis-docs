const baseUrl = {
    data() {
        return {
            baseUrl: '',
        }
    },
    mounted() {
        if (process.env.NODE_ENV == "development") {
            // console.log('development');
        } else if (process.env.NODE_ENV == "production") {
            // console.log('production');
            this.baseUrl = process.env.VUE_APP_API_PUBLIC_PATH
        }
    },
}
export default baseUrl