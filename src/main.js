import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Antd from "ant-design-vue";
import "ant-design-vue/dist/antd.css";
import '@/assets/css/markdown.less'
import '@/assets/css/reset.less'
import '@/assets/css/base.less'

// import '@/utils/rem'

import { VueBus } from "jpaas-common-lib";
Vue.use(VueBus);

import VMdEditor from '@kangc/v-md-editor';
import '@kangc/v-md-editor/lib/style/base-editor.css';
import githubTheme from '@kangc/v-md-editor/lib/theme/github.js';
import '@kangc/v-md-editor/lib/theme/style/github.css';
import hljs from 'highlight.js';
VMdEditor.use(githubTheme, {
  Hljs: hljs,
});
Vue.use(VMdEditor);

// 使用样式，有多种样式可选
import "highlight.js/styles/an-old-hope.css";
// 增加自定义命令v-highlight
Vue.directive("highlight", function(el) {
  let blocks = el.querySelectorAll("pre code");
  blocks.forEach(block => {
    hljs.highlightBlock(block);
  });
});
// 增加组定义属性，用于在代码中预处理代码格式
Vue.prototype.$hljs = hljs;

import VueClipBoard from 'vue-clipboard2' //剪切板
Vue.use(VueClipBoard)

import vueSeamlessScroll from 'vue-seamless-scroll' //无缝滚动
Vue.use(vueSeamlessScroll)

import BaiduMap from 'vue-baidu-map-v3'
Vue.use(BaiduMap, {
	ak: 'y1WzWkA8jbROq28QsyXZVqzn'
})

import MdPreview from '@/components/mdPreview.vue'
Vue.component('MdPreview',MdPreview)
import MdCode from '@/components/mdCode.vue'
Vue.component('MdCode',MdCode)

import VisGridLayout from '@/components/visGridLayout.vue'
Vue.component('VisGridLayout',VisGridLayout)

import mixin from '@/mixins/md.js'
Vue.mixin(mixin)
import baseUrl from '@/mixins'
Vue.mixin(baseUrl)

/*右键菜单*/
import contentmenu from 'v-contextmenu'
import 'v-contextmenu/dist/index.css'
Vue.use(contentmenu)

import layer from 'rx-vue-layer'
import 'rx-vue-layer/lib/vue-layer.css';
Vue.prototype.$layer = layer(Vue);

Vue.config.productionTip = false;
Vue.prototype.$env= process.env
Vue.use(Antd);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
