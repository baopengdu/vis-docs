import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    appSetting: {
      user: {
        userId: '1'
      }, //当前用户
      appKey: "",//应用标识
      menus: [],//所有菜单
      selectMenu: {},//选中的菜单
      buttons: {}, //当前用户权限 应用按钮
      navigation: [{key: 'home-index', title: '首页', collapsed: false}],//面包屑导航
      activeKey: '',//导航栏当 前选中相 ;
      showHome: true,//展示方式
      appId: "",//应用ID
      appName: "",//应用名称
      showType: 'window',//列表按钮窗口类型 window弹窗，newPage新页面；
      routers: [], //路由
      allButtons: {},//所有菜单按钮
      idKey: '1',//权限的key,20220315改为和appKey一致了
      menuMap: [],
      menuPath: [],
      leftMenu: [],
      newPageMenu: "", //保存新窗口打开的页面 防止死循环
      selectTopMenuKey: [''],// 选中的顶部菜单，用于代替idKey(20220316)
    },
    curMenu: ''
  },
  mutations: {
    getMenuData(state, newVal) {
      state.curMenu = newVal
      localStorage.setItem('curMenu', newVal)
    }
  },
  actions: {
  },
  modules: {
  }
})
