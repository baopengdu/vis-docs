var baseURL = process.env.VUE_APP_API_CTX_PATH;

module.exports ={
    //关闭eslint
    lintOnSave: false,
    //解决build打包的时候 dist文件里面css、js、img路径错误错误的问题
    publicPath: './',
    assetsDir: 'static',
    css: {
        loaderOptions: {
            // postcss: {
            //     plugins: [
            //         require('postcss-pxtorem')({
            //             rootValue        : 16,
            //             unitPrecision    : 5,
            //             propList         : ['*', '!border', '!letter-spacing', '!box-shadow', '!border-radius'],
            //             selectorBlackList: ['un-rem'],
            //             minValue         : 3,
            //             exclude          : /node_modules/i
            //         })
            //     ]
            // }
        }
    },
}