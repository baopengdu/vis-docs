module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: [
    ["prismjs", {
      languages: ["html", "javascript", "css", "markup", "json", "java", "sql", "nginx", "php", "c", "bash", "dart", "cpp", "python", "go"],
      plugins: ["line-numbers", "show-language", "copy-to-clipboard"],
      theme: "tomorrow", //default, tomorrow
      css: true
    }]
  ]
}